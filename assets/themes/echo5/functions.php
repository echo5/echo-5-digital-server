<?php

/**
 * Allow rest comments
 */
add_filter( 'rest_allow_anonymous_comments', '__return_true' );


/**
 * Filter page and post links
 */
add_filter( 'post_link', 'echo5_filter_permalinks', 10, 3);
add_filter( 'page_link', 'echo5_filter_permalinks', 10, 3);
add_filter( 'post_type_link', 'echo5_filter_permalinks', 10, 3);
// add_filter( 'category_link', 'echo5_filter_permalinks', 11, 3);
// add_filter( 'tag_link', 'echo5_filter_permalinks', 10, 3);
// add_filter( 'author_link', 'echo5_filter_permalinks', 11, 3);
// add_filter( 'day_link', 'echo5_filter_permalinks', 11, 3);
// add_filter( 'month_link', 'echo5_filter_permalinks', 11, 3);
// add_filter( 'year_link', 'echo5_filter_permalinks', 11, 3);
function echo5_filter_permalinks($permalink, $post) {
    return str_replace(WP_SITEURL, WP_FRONTENDURL, $permalink);
}

/**
 * Allow all CORS.
 *
 * @since 1.0.0
 */
function wp_rest_allow_all_cors() {
	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
	add_filter( 'rest_pre_serve_request', function( $value ) {
		header( 'Access-Control-Allow-Origin: *' );
		header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
		header( 'Access-Control-Allow-Credentials: true' );
		return $value;
	});
}
add_action( 'rest_api_init', 'wp_rest_allow_all_cors', 15 );

/**
 * Remove p tags in excerpt
 */
remove_filter ('the_excerpt',  'wpautop');

/**
 * Add editor styles
 */
add_editor_style( array( 'assets/css/editor-style.css' ) );

/**
 * Enable comment shortcodes
 */
add_filter( 'comment_text', 'do_shortcode' ); 

/**
 * Code shortcode
 */
function echo5_code_shortcode($atts, $content = '') {
	return '<pre><code>'.$content.'</code></pre>';
}
add_shortcode('code', 'echo5_code_shortcode');

/**
 * Remove p tags in excerpt
 */
remove_filter ('the_excerpt',  'wpautop');
